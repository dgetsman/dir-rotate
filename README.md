# dir-rotate

In the same basic manner as logrotate, dir-rotate will rotate, optionally 
archive, and optionally delete a directory, and its contents, in order to 
preserve directory usability, organization, and drive space.

## Feature set

This project is only just starting out, but here is a list of features that
will, ideally, be present in the final product.

  * rotation of directories; this will consist of the following:
  * * move old directory from _dirspec_ to _dirspec.1_ (copy in order to facilitate ease of some of the following)
  * * optionally zero out all of the original files after copy is complete, in order to keep logging facilities happy if they require the open file handle
  * * optionally archive the backed up _dirspec_ (optionally after _x_ iterations)
  * * optionally delete the archived _dirspec_ or _dirspec.tar.whatever_ archive after _x_ iterations
  * definition of one's own archival utilities and compressors

Please note that I am most definitely open to new feature suggestions, this is just the barest of skeletons for this project.